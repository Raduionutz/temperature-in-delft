import json
import requests
import time

url = 'https://weerindelft.nl/clientraw.txt?{unix_time}'.format(unix_time=str(time.time()))

print(str(requests.get(url).content).split(' ')[4] + ' degrees Celsius')
