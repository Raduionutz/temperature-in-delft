FROM python:3

ADD HoeWarmIsHetInDelft.py /
ADD requirements.txt /

RUN pip install -r requirements.txt

CMD [ "python", "./HoeWarmIsHetInDelft.py" ]
